<?php

define('THEME_URI', get_template_directory_uri());
define('THEME_PATH', get_template_directory());

include 'inc/post_types.php';

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('app-js', THEME_URI . '/assets/js/app.js', ['jquery'], time(), true);

    wp_localize_script('app-js', 'theme_ajax',
        [
            'url'       => admin_url('admin-ajax.php'),
            'theme_uri' => THEME_URI,
        ]
    );
});

add_action('wp_ajax_nopriv_portfolio_comment', 'portfolio_comment');
add_action('wp_ajax_portfolio_comment', 'portfolio_comment');

function portfolio_comment()
{
    $post_id = $_POST['post_id'];
    $email = $_POST['email'];
    $comment = $_POST['comment'];
    $comments = get_field('comments', $post_id);
    $comments[] = [
        'email'   => $email,
        'comment' => $comment,
    ];
    $update = update_field('comments', $comments, $post_id);
    wp_send_json(['message' => $update]);
}