document.addEventListener("DOMContentLoaded", function () {
    (() => {
        var form = document.querySelector('.portfolio_comment_form')
        if (form) {
            form.onsubmit = (e) => {
                e.preventDefault();

                var post_id = e.target.elements.cu_post_id.value,
                    email = e.target.elements.cu_user_email.value,
                    comment = e.target.elements.cu_user_comment.value,
                    data = new FormData()

                if (email && comment) {
                    data.append('action', "portfolio_comment");
                    data.append('post_id', post_id);
                    data.append('email', email);
                    data.append('comment', comment);

                    (async () => {
                        var response = await fetch(theme_ajax.url, {
                            method: 'POST',
                            body  : data
                        });
                        var result = await response.json();
                        if (result !== null && result.message) {
                            alert('Comment sent.')
                        } else {
                            alert('An error occurred while sending.')
                        }
                    })();
                } else {
                    alert('Empty fields.')
                }
            }
        }
    })();
});