<?php

add_action('init', function () {
    register_post_type('portfolio', [
        'labels'        => [
            'name'               => 'Portfolio',
            'singular_name'      => 'Portfolio',
            'add_new'            => 'Add Portfolio',
            'add_new_item'       => 'Add Portfolio',
            'edit_item'          => 'Edit Portfolio',
            'new_item'           => 'New Portfolio',
            'view_item'          => 'View Portfolio',
            'search_items'       => 'Search Portfolio',
            'not_found'          => 'Not found',
            'not_found_in_trash' => 'Not found in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Portfolio',
        ],
        'public'        => true,
        'menu_position' => 20,
        'menu_icon'     => 'dashicons-pressthis',
        'hierarchical'  => false,
        'show_in_rest'  => true,
        'supports'      => ['title'],
        'has_archive'   => true,
    ]);
});