<?php get_header(); ?>
    <form class="portfolio_comment_form">
        <input type="hidden" name="cu_post_id" value="<?php echo get_the_ID() ?>">
        <input type="email" name="cu_user_email" placeholder="Email"><br>
        <textarea name="cu_user_comment" cols="30" rows="10" placeholder="Comment"></textarea><br>
        <button>Send</button>
    </form>
<?php get_footer();